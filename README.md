# Unified Django and Flask App with shared Postgres Volumes
We've taken our previous sample django and flask app and unified them with docker-compose to run all together.


### Usage

1. Rename *.env.dev-sample* to *.env.dev* and *.env.flask.dev-sample* to *.env.flask.dev* 
2. Update the environment variables in the *docker-compose.yml*, *.env.flask.dev* and *.env.dev* files(Optional).
3. Build the images and run the containers:

    ```sh
    $ docker-compose up -d --build
    ```

    Test out the django app at [http://localhost:8000](http://localhost:8000). and the flask app can be accesible at  [http://localhost:5000](http://localhost:5000)

We've create a shared postgres DB environment also to use in the app and added *pgadmin4* to access the database.

To Connect to the database, follow the below mentioned screenshots:

pgadmin is available at  [http://localhost:16543](http://localhost:16543) , use email: `test@gmail.com` and password: `test123!` to login to the admin interface.

- Create a server from 'Add New Server' button in the dashboard

<img src="https://i.imgur.com/1GwseJM.png" />

- in "General" Tab, give `name: root_user` and go to the connection tab

<img src="https://i.imgur.com/0YPfbu5.png" />

- Give our default database parameters as follows:

<img src="https://i.imgur.com/DcxkiNb.png" />

- use database name `hello_django_prod` instead of `hello_django_dev`

- If avobe information is given correctly, we can access our database or create a new one if required.

<img src="https://i.imgur.com/3MnwG0W.png" />

