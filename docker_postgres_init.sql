CREATE USER hello_django WITH PASSWORD 'hello_django' CREATEDB;
CREATE DATABASE hello_django_prod
    WITH 
    OWNER = hello_django
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.utf8'
    LC_CTYPE = 'en_US.utf8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;